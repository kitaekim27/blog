import { defineConfig } from "astro/config";
import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  markdown: {
    shikiConfig: {
      theme: "css-variables",
      wrap: true
    }
  },
  integrations: [sitemap()],
  site: "https://blog.tinkeringallday.com"
});

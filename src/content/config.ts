import { z, defineCollection } from "astro:content";

export const collections = {
  "posts": defineCollection({
    type: "content",
    schema: z.object({
      date: z.date(),
      summary: z.string().optional(),
      tags: z.array(z.string()).optional(),
      title: z.string(),
    })
  }),
};
